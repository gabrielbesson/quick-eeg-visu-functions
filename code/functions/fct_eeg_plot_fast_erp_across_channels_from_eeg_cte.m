function [erp_t,pval_t_a0] = fct_eeg_plot_fast_erp_across_channels_from_eeg_cte(times, erp_cte, prm)
% function [erp_t,pval_t_a0] = fct_eeg_plot_fast_erp_across_channels_from_eeg_cte(times, erp_cte, prm)
%
% SEE ALSO fct_eeg_plot_fast_erp_across_channels_from_EEGstruct, fct_eeg_plot_sig

def_mycolors;

if ~exist('prm','var'),  prm = struct;  end
if ~isfield(prm,'color'),            prm.color = bleu;  end
if ~isfield(prm,'lineW'),            prm.lineW = 2;  end
if ~isfield(prm,'linealpha'),        prm.linealpha = .1;     end
% if ~isfield(prm,'reduc_fun'),        prm.reduc_fun = 'mean';     end % 'mean'  'median' 
% if ~isfield(prm,'affiche_ci'),       prm.affiche_ci = 1;         end
% if ~isfield(prm,'meth_ci'),          prm.meth_ci = 'ste';        end % 'ste'  'quartile'  '95p_med'  '95p_m'
% if ~isfield(prm,'affiche_single'),   prm.affiche_single = 0;     end
% if ~isfield(prm,'single_color'),     prm.single_color = gray7;     end
% if ~isfield(prm,'single_linealpha'), prm.single_linealpha = 0.5;     end
% if ~isfield(prm,'P1_t'),             prm.P1_t = find(EEG_epoched.times>100,1);  end
% if ~isfield(prm,'lineT'),            prm.lineT = '-';  end
% if ~isfield(prm,'facealpha'),        prm.facealpha = .2;  end

if isfield(prm,'xlims'),  xlims = prm.xlims;  else xlims = [];  end
if isfield(prm,'ylims'),  ylims = prm.ylims;  else ylims = [];  end

if ~isfield(prm,'aff_diff_a0'),        prm.aff_diff_a0 = false;  end
if ~isfield(prm,'meth_cmp'),           prm.meth_cmp = 'anova';  end %  'anova'  'rank'
if ~isfield(prm,'y_dec'),              prm.y_dec = 0;  end
if ~isfield(prm,'y_pos_diff_a0'),      prm.y_pos_diff_a0 = 0; end % ylims(1)+diff(ylims)/100;  
if ~isfield(prm,'color_diff_a0'),      prm.color_diff_a0 = orange;  end


% fct_orient_erp = @(x,t) repmat(sign(x(:,t,:)),1,size(x,2),1).*x;

% % EEG_epoch
% times = EEG_epoch.times;
% erp_cte = EEG_epoch.data;
erp_ct = mean(erp_cte,3,'omitnan');

Nc = size(erp_cte,1);
Nt = size(erp_cte,2);
Ne = size(erp_cte,3);


hold on;

if ~isempty(xlims),  xlim(xlims);  fct_plot_horz_axis(0,'-',[],1);  end
if ~isempty(ylims),  ylim(ylims);  fct_plot_vert_axis(0,'-',[],1);  end




% Plot: each channel
for c = 1:Nc
    %plot1 = 
    plot(times, erp_ct(c,:), 'color', [prm.color prm.linealpha], 'linewidth', prm.lineW);
    %plot1.Color(4) = prm.single_linealpha;
end



% % Applique la réduction sur la dimension 1
% if ~isequal(prm.reduc_fun(1:3),'nan')
%     prm.reduc_fun = ['nan' prm.reduc_fun];
% end
% if numel(prm.reduc_fun)>numel('_orientch') && isequal(prm.reduc_fun(end-numel('_orientch')+1:end), '_orientch')
%     prm.reduc_fun = prm.reduc_fun(1:end-numel('_orientch'));
%     erp_t = fct_orient_erp(feval(prm.reduc_fun, erp_cte, 1),prm.P1_t);
% else
%     erp_t = feval(prm.reduc_fun, erp_cte, 1);
% end
% 
% 
% 
% % Affichage ERPs (avec ou sans CI)
% if ~isequal(prm.color,'none')
%     if prm.affiche_ci && ~isequal(prm.meth_ci,'none')
%             clear param
%             param.col = prm.color;
%             param.facealpha = prm.facealpha;
%             param.lineW = prm.lineW;
%             param.lineT = prm.lineT;
%         fct_plot_with_ci(times, erp_t, fct_calc_CI(erp_cte, prm.meth_ci), param);
%     else
%         plot(times, erp_t, prm.lineT, 'color', prm.color, 'linewidth', prm.lineW);
%     end
% end
% 
% 
% 
% 
% Fig : plots significativé entre ERPs et 0
if prm.aff_diff_a0
        pval_t_a0 = fct_eeg_stat_cmp2(erp_ct, zeros(size(erp_ct)), prm.meth_cmp, 1);
            param.col=prm.color_diff_a0;
            param.lineW = 5;
    fct_eeg_plot_sig(pval_t_a0, times, prm.y_pos_diff_a0 + prm.y_dec, param);
end

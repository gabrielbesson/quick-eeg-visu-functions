function fct_eeg_plot_fast_topo_from_topo_c(topo_c, EEG_chanlocs, prm)
% function fct_eeg_plot_fast_topo_from_topo_c(topo_c, EEG_chanlocs, prm)
%
% SEE ALSO fct_eeg_plot_fast_topo_from_EEGstruct, topoplot

def_mycolors;
bkg_col = [0.94 0.94 0.94];

if ~exist('prm','var'),  prm = struct;  end
if ~isfield(prm,'cmap'),            prm.cmap = cmap_bi;  end
if ~isfield(prm,'clims'),           prm.clims = 17*[-1 1];  end
if ~isfield(prm,'n_contour'),       prm.n_contour = 16;  end
if ~isfield(prm,'electrodes'),      prm.electrodes = 'on';  end
if ~isfield(prm,'col_circle'),      prm.col_circle = bkg_col;  end

% if ~isfield(prm,'lineW'),            prm.lineW = 2;  end
% if ~isfield(prm,'linealpha'),        prm.linealpha = .2;     end
% if ~isfield(prm,'affiche_ci'),       prm.affiche_ci = 1;         end
% if ~isfield(prm,'meth_ci'),          prm.meth_ci = 'ste';        end % 'ste'  'quartile'  '95p_med'  '95p_m'
% if ~isfield(prm,'affiche_single'),   prm.affiche_single = 0;     end
% if ~isfield(prm,'single_color'),     prm.single_color = gray7;     end
% if ~isfield(prm,'single_linealpha'), prm.single_linealpha = 0.5;     end
% if ~isfield(prm,'P1_t'),             prm.P1_t = find(EEG_epoched.times>100,1);  end
% if ~isfield(prm,'lineT'),            prm.lineT = '-';  end
% if ~isfield(prm,'facealpha'),        prm.facealpha = .2;  end
% if ~isfield(prm,'aff_diff_a0'), prm.aff_diff_a0 = 1;  end
% if ~isfield(prm,'meth_cmp'), prm.meth_cmp = 'anova';  end %  'anova'  'rank'
% if ~isfield(prm,'y_dec'), prm.y_dec = 0;  end
% if isfield(prm,'xlims'),  xlims = prm.xlims;  else xlims = xlim;  end
% if isfield(prm,'ylims'),  ylims = prm.ylims;  else ylims = ylim;  end

% if ~isfield(prm,'y_pos_diff_a0'), prm.y_pos_diff_a0 = ylims(1)+diff(ylims)/100;  end
% if ~isfield(prm,'color_diff_a0'),            prm.color_diff_a0 = gray3;  end



hold on;

topoplot(double(topo_c), EEG_chanlocs ...
               ,'maplimits', prm.clims ...
               ,'conv', 'off' ...
               ,'colormap', prm.cmap ...
               ,'style', 'both' ...
               ,'electrodes', prm.electrodes ...
               ,'numcontour', linspace(prm.clims(1),prm.clims(2),prm.n_contour) ...
               ); % 'fill'  'map'
%                 cols_shift = 1; % starting from 1
%             cur_col = cmap_paired(mod( (r-1)*2+cols_shift, size(cmap_paired,1) )+1,:);
%             set(findobj(gca,'type','patch'),'facecolor',cur_col)
set(findobj(gca,'type','patch'),'facecolor', prm.col_circle)
set(gcf, 'Color', bkg_col);


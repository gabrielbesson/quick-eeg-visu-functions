function [erp_ct,pval_t_a0] = fct_eeg_plot_fast_erp_across_channels_from_EEGstruct(EEG_epoch, samples_oi_ce, prm)
% function [erp_ct,pval_t_a0] = fct_eeg_plot_fast_erp_across_channels_from_EEGstruct(EEG_epoch, samples_oi_ce, prm)
%
%
% Function to quickly plot the ERP of an EEGLAB structure across channels.
%
% 
% INPUTS
%       - EEG_epoch             EEGLAB structure containing epoched data (size(EEG_epoch,3)>1)
%       - samples_oi_ce         1x2 cell array indicating the samples of interest (element 1: channels, element 2: epochs). 
%                                   Could be a list of integer (the indices of interest) or the string 'all' (all arethen kept of interesst).
%                                   Default: {'all' 'all'}.
%       - prm                   parameters supplied in a structure with fields:
%               .condition_field string (1xN char array) indicating the field in .epoch to indicate the samples of interest. ('')
%               .color           color of the ERP for each channel to plot. (bleu)
%               .lineW           line width of the ERP for each channel to plot. (2)
%               .linealpha       line alpha of the ERP for each channel to plot. (100)
%               .aff_diff_a0     difference to 0 - boolean indcating whether to compute and display it. (false)
%               .meth_cmp        difference to 0 - method to do the comparison. (anova)
%               .y_pos_diff_a0   difference to 0 - y-coordinate to plot it. (0)
%               .y_offset        difference to 0 - offset on y to add to .y_pos_diff_a0. (0)
%               .color_diff_a0   difference to 0 - color to plot it. (orange)
%
% OUTPUTS
%       - erp_ct       ERP (time series) of each channel of interest
%       - pval_t_a0    difference to 0 - p-values of the statistical test (requires prm.aff_diff_a0=true)
% 
% 
% DEPENDENCIES
%       - fct_eeg_plot_sig.
% 
%
% Gabriel Besson, 2023, University of Coimbra.
%
% 
% VERSION UPDATES
%   - 2023-06-19.   Initial development. / First versioning.
% 
%
% SEE ALSO fct_eeg_plot_fast_erp_across_channels_from_eeg_cte, fct_eeg_plot_sig

% def_mycolors;
blue = [0 .36863 .6902];
orange = [.86078 .43922 .11176];


if ~exist('prm','var'),  prm = struct;  end
if ~isfield(prm,'color'),            prm.color = blue;  end
if ~isfield(prm,'lineW'),            prm.lineW = 2;  end
if ~isfield(prm,'linealpha'),        prm.linealpha = .1;     end
% if ~isfield(prm,'reduc_fun'),        prm.reduc_fun = 'mean';     end % 'mean'  'median' 
% if ~isfield(prm,'affiche_ci'),       prm.affiche_ci = 1;         end
% if ~isfield(prm,'meth_ci'),          prm.meth_ci = 'ste';        end % 'ste'  'quartile'  '95p_med'  '95p_m'
% if ~isfield(prm,'affiche_single'),   prm.affiche_single = 0;     end
% if ~isfield(prm,'single_color'),     prm.single_color = gray7;     end
% if ~isfield(prm,'single_linealpha'), prm.single_linealpha = 0.5;     end
% if ~isfield(prm,'P1_t'),             prm.P1_t = find(EEG_epoched.times>100,1);  end
% if ~isfield(prm,'lineT'),            prm.lineT = '-';  end
% if ~isfield(prm,'facealpha'),        prm.facealpha = .2;  end

if ~isfield(prm,'aff_diff_a0'),        prm.aff_diff_a0 = false;  end
if ~isfield(prm,'meth_cmp'),           prm.meth_cmp = 'anova';  end %  'anova'  'rank'
if ~isfield(prm,'y_offset'),              prm.y_offset = 0;  end
if ~isfield(prm,'y_pos_diff_a0'),      prm.y_pos_diff_a0 = 0; end % ylims(1)+diff(ylims)/100;  
if ~isfield(prm,'color_diff_a0'),      prm.color_diff_a0 = orange;  end
if ~isfield(prm,'condition_field'),    prm.condition_field = '';  end


% fct_orient_erp = @(x,t) repmat(sign(x(:,t,:)),1,size(x,2),1).*x;

% assert(numel(EEG_epoch)==1, 'EEG_epoch is an array, should be one element.');
assert( all(EEG_epoch(1).nbchan==[EEG_epoch.nbchan]), 'Different elements of ''EEG_epoch'' have different number of channels.' );

Np = numel(EEG_epoch);
Nc = size(EEG_epoch(1).data,1);
% Nt = size(EEG_data_cte,2);
% Ne = size(EEG_data_cte,3);


if ~exist('samples_oi_ce','var') || isempty(samples_oi_ce)
    samples_oi_c = 1:Nc;
    samples_oi_e = 'all';
else
    if isequal(samples_oi_ce{1},'all')
        samples_oi_c = 1:Nc;
    elseif isnumeric(samples_oi_ce{1})
        samples_oi_c = samples_oi_ce{1};
    else
        error('error');
    end
    

    if ischar(samples_oi_ce{2}) || isnumeric(samples_oi_ce{2})
        samples_oi_e = samples_oi_ce{2};
    else
        error('The second field of ''samples_oi_ce'' should be char or numeric values');
    end
end



% EEG_epoch
if Np==1
    times = EEG_epoch.times;
    erp_cte = EEG_epoch.data(samples_oi_c,:,samples_oi_e);
    erp_ct = mean(erp_cte,3,'omitnan');
else
    times = EEG_epoch(1).times;
    if isequal(samples_oi_e,'all')
        erp_ct_p = arrayfun(@(c) mean(c.data(samples_oi_c,:,:),3,'omitnan'), EEG_epoch,'uni',0);
    elseif isnumeric(samples_oi_e)
        erp_ct_p = arrayfun(@(c) mean(c.data(samples_oi_c,:,samples_oi_e),3,'omitnan'), EEG_epoch,'uni',0);
    elseif ~isempty(prm.condition_field) % ischar(samples_oi_e)
        erp_ct_p = cell(Np,1);
        for p = 1:Np
            eoi = find(ismember({EEG_epoch(p).epoch.(prm.condition_field)}, samples_oi_e));
            erp_ct_p{p} = mean(EEG_epoch(p).data(samples_oi_c,:,eoi),3,'omitnan');
        end
    end
    erp_ctp = cat(3,erp_ct_p{:});
    erp_ct = mean(erp_ctp,3,'omitnan');
end



hold on;

% Plot: each channel
for c = 1:size(erp_ct,1)
    %plot1 = 
    plot(times, erp_ct(c,:), 'color', [prm.color prm.linealpha], 'linewidth', prm.lineW);
    %plot1.Color(4) = prm.single_linealpha;
end


if isfield(prm,'xlims'),  xlims = prm.xlims;  else xlims = xlim;  end
if isfield(prm,'ylims'),  ylims = prm.ylims;  else ylims = ylim;  end

xlim(xlims); ylim(ylims);
xline(0,'color', [1 1 1]*.7, 'linewidth',1); % fct_plot_vert_axis(0,'-',[],1);
yline(0,'color', [1 1 1]*.7, 'linewidth',1); % fct_plot_horz_axis(0,'-',[],1);


set(gca,'tickdir','out');

ylabel('EEG activity (µV)');
xlabel('time (ms)');


% % Applique la réduction sur la dimension 1
% if ~isequal(prm.reduc_fun(1:3),'nan')
%     prm.reduc_fun = ['nan' prm.reduc_fun];
% end
% if numel(prm.reduc_fun)>numel('_orientch') && isequal(prm.reduc_fun(end-numel('_orientch')+1:end), '_orientch')
%     prm.reduc_fun = prm.reduc_fun(1:end-numel('_orientch'));
%     erp_t = fct_orient_erp(feval(prm.reduc_fun, erp_cte, 1),prm.P1_t);
% else
%     erp_t = feval(prm.reduc_fun, erp_cte, 1);
% end
% 
% 
% 
% % Affichage ERPs (avec ou sans CI)
% if ~isequal(prm.color,'none')
%     if prm.affiche_ci && ~isequal(prm.meth_ci,'none')
%             clear param
%             param.col = prm.color;
%             param.facealpha = prm.facealpha;
%             param.lineW = prm.lineW;
%             param.lineT = prm.lineT;
%         fct_plot_with_ci(times, erp_t, fct_calc_CI(erp_cte, prm.meth_ci), param);
%     else
%         plot(times, erp_t, prm.lineT, 'color', prm.color, 'linewidth', prm.lineW);
%     end
% end
% 
% 
% 
% 
% Fig : plots significativé entre ERPs et 0
if prm.aff_diff_a0
        pval_t_a0 = fct_eeg_stat_cmp2(erp_ct, zeros(size(erp_ct)), prm.meth_cmp, 1);
            param.col=prm.color_diff_a0;
            param.lineW = 5;
    fct_eeg_plot_sig(pval_t_a0, times, prm.y_pos_diff_a0 + prm.y_offset, param);
end

function fct_eeg_plot_fast_eegplot_from_EEGstruct(EEG, tlims, prm)
% function fct_eeg_plot_fast_eegplot_from_EEGstruct(EEG, times_oi, prm)
%
% SEE ALSO eegplot

if numel(EEG)>1
    warning('The EEGLAB structure contained more than 1 element (N=%d). The first is retained.', numel(EEG));
    EEG = EEG(1);
end

% def_mycolors;

if ~exist('tlims','var'),  tlims = EEG.times([1 end]);  end
if ~exist('prm','var'),  prm = struct;  end
if ~isfield(prm,'EEG2'),        prm.EEG2 = [];     end
if ~isfield(prm,'winlength'),            prm.winlength = 15;  end
if ~isfield(prm,'spacing'),            prm.spacing = 80;  end
if ~isfield(prm,'butlabel'),            prm.butlabel = 'Select time ranges';  end

% if ~isfield(prm,'cmap'),            prm.cmap = cmap_bi;  end
% if ~isfield(prm,'clims'),            prm.clims = 17*[-1 1];  end
% if ~isfield(prm,'n_contour'),            prm.n_contour = 16;  end
% if ~isfield(prm,'lineW'),            prm.lineW = 2;  end
% if ~isfield(prm,'linealpha'),        prm.linealpha = .2;     end
% if ~isfield(prm,'affiche_ci'),       prm.affiche_ci = 1;         end
% if ~isfield(prm,'meth_ci'),          prm.meth_ci = 'ste';        end % 'ste'  'quartile'  '95p_med'  '95p_m'
% if ~isfield(prm,'affiche_single'),   prm.affiche_single = 0;     end
% if ~isfield(prm,'single_color'),     prm.single_color = gray7;     end
% if ~isfield(prm,'single_linealpha'), prm.single_linealpha = 0.5;     end
% if ~isfield(prm,'P1_t'),             prm.P1_t = find(EEG_epoched.times>100,1);  end
% if ~isfield(prm,'lineT'),            prm.lineT = '-';  end
% if ~isfield(prm,'facealpha'),        prm.facealpha = .2;  end
% if ~isfield(prm,'aff_diff_a0'), prm.aff_diff_a0 = 1;  end
% if ~isfield(prm,'meth_cmp'), prm.meth_cmp = 'anova';  end %  'anova'  'rank'
% if ~isfield(prm,'y_dec'), prm.y_dec = 0;  end
% if isfield(prm,'xlims'),  xlims = prm.xlims;  else xlims = xlim;  end
% if isfield(prm,'ylims'),  ylims = prm.ylims;  else ylims = ylim;  end

% if ~isfield(prm,'y_pos_diff_a0'), prm.y_pos_diff_a0 = ylims(1)+diff(ylims)/100;  end
% if ~isfield(prm,'color_diff_a0'),            prm.color_diff_a0 = gray3;  end


% Select the timepoints of interest
EEG = pop_select(EEG,'time',tlims); % done like that to crop the events too

% Const
Nc = size(EEG.data,1);
Nt_init = size(EEG.data,2);
Ne = size(EEG.data,3);

EEG_data_cte = EEG.data;
Nt = size(EEG_data_cte,2);

% Handling 'prm.EEG2'
if isempty(prm.EEG2)
    EEG_data2_cte = EEG_data_cte * nan;
else
    if isstruct(prm.EEG2)
        if numel(prm.EEG2)>1
            warning('The EEGLAB structure in ''prm.EEG2'' contained more than 1 element (N=%d). The first is retained.', numel(prm.EEG2));
            prm.EEG2 = prm.EEG2(1);
        end
        assert(size(EEG_data2_cte,1)==Nc && size(EEG_data2_cte,2)==Nt_init && size(EEG_data2_cte,3)==Ne);
        prm.EEG2 = pop_select(prm.EEG2,'time',tlims);
        EEG_data2_cte = prm.EEG2.data;
    elseif isnumeric(prm.EEG2)
        EEG_data2_cte = prm.EEG2;
        if size(EEG_data2_cte,2)==Nt_init
            times_oi = find(prm.EEG2.times>=tlims(1) & prm.EEG2.times<=tlims(2));
            EEG_data2_cte = EEG_data2_cte(:,times_oi,:);
        end
    end
    assert(size(EEG_data2_cte,1)==Nc && size(EEG_data2_cte,2)==Nt && size(EEG_data2_cte,3)==Ne);
end


% Scroll data - (if asked) with EEG_data2_cte on top of it 
eegplot(EEG_data_cte, 'srate', EEG.srate ...
                    , 'eloc_file', EEG.chanlocs ...INFOS.chan.locs_rsrc_f_nm ... {EEG(p).chanlocs.labels} ...
                    , 'events', EEG.event ...
                    , 'title', EEG.subject ...
                    , 'winlength', prm.winlength ...
                    , 'spacing', prm.spacing ...
                    , 'data2', EEG_data2_cte ...
                    ..., 'ctrlselectcommand', {'disp(gcbf); eegplot(''topoplot'',   gcbf);' '' ''} ...
                    ..., 'command', command ...
                    , 'butlabel', prm.butlabel ...
                   );


set(gcf, 'Color', [0.94 0.94 0.94]);



function fct_eeg_plot_fast_topo_from_EEGstruct(EEG, samples_oi_te, prm)
% function fct_eeg_plot_fast_topo_from_EEGstruct(EEG, samples_oi_te, prm)
%
%
% Function to quickly plot the topoplot of an EEGLAB structure.
%
% 
% INPUTS
%       - EEG                   EEGLAB structure containing epoched data (size(EEG_epoch,3)>1)
%       - samples_oi_te         1x2 cell array indicating the samples of interest (element 1: timpoints, element 2: epochs). 
%                                   Could be a list of integer (the indices of interest) or the string 'all' (all are then kept of interesst).
%                                   Default: {'all' 'all'}.
%       - prm                   parameters supplied in a structure with fields:
%               .avg_mth         string indicating the function to use to average. ('mean'), 'median', etc.
%               .condition_field string (1xN char array) indicating the field in .epoch to indicate the samples of interest. ('')
%               .cmap            line width of the ERP for each channel to plot. (2)
%               .clims           line alpha of the ERP for each channel to plot. (100)
%               .n_contour       difference to 0 - boolean indcating whther to compute and display it. (false)
%               .electrodes      difference to 0 - method to do the comparison. (anova)
%               .col_circle      difference to 0 - y-coordinate to plot it. (0)
%
% OUTPUTS
%       - erp_ct       ERP (time series) of each channel of interest
%       - pval_t_a0    difference to 0 - p-values of the statistical test (requires prm.aff_diff_a0=true)
% 
% 
% DEPENDENCIES
%       - topoplot
%       - (brewermap, (c) 2017 Stephen Cobeldick)
%              To allow the use of the colormap "*RdBu". Use 'Paurula' otherwise.
%              
%
% Gabriel Besson, 2023, University of Coimbra.
%
% 
% VERSION UPDATES
%   - 2023-06-19.   Initial development. / First versioning.
% 
%
% SEE ALSO fct_eeg_plot_fast_topo_from_topo_c, topoplot

% def_mycolors;
bkg_col = [0.94 0.94 0.94];
if exist('brewermap','file')
    cmap_bi=brewermap(64,'*RdBu'); % 'parula'
else
    cmap_bi=colormap('parula');
end

if ~exist('prm','var'),  prm = struct;  end
if ~isfield(prm,'avg_mth'),        prm.avg_mth = 'mean';     end % 'mean'  'median' 
if ~isfield(prm,'cmap'),           prm.cmap = cmap_bi;  end
if ~isfield(prm,'clims'),          prm.clims = 17*[-1 1];  end
if ~isfield(prm,'n_contour'),      prm.n_contour = 16;  end
if ~isfield(prm,'electrodes'),     prm.electrodes = 'on';  end
if ~isfield(prm,'col_circle'),     prm.col_circle = bkg_col;  end
if ~isfield(prm,'condition_field'),    prm.condition_field = '';  end

% if ~isfield(prm,'lineW'),            prm.lineW = 2;  end
% if ~isfield(prm,'linealpha'),        prm.linealpha = .2;     end
% if ~isfield(prm,'affiche_ci'),       prm.affiche_ci = 1;         end
% if ~isfield(prm,'meth_ci'),          prm.meth_ci = 'ste';        end % 'ste'  'quartile'  '95p_med'  '95p_m'
% if ~isfield(prm,'affiche_single'),   prm.affiche_single = 0;     end
% if ~isfield(prm,'single_color'),     prm.single_color = gray7;     end
% if ~isfield(prm,'single_linealpha'), prm.single_linealpha = 0.5;     end
% if ~isfield(prm,'P1_t'),             prm.P1_t = find(EEG_epoched.times>100,1);  end
% if ~isfield(prm,'lineT'),            prm.lineT = '-';  end
% if ~isfield(prm,'facealpha'),        prm.facealpha = .2;  end
% if ~isfield(prm,'aff_diff_a0'), prm.aff_diff_a0 = 1;  end
% if ~isfield(prm,'meth_cmp'), prm.meth_cmp = 'anova';  end %  'anova'  'rank'
% if ~isfield(prm,'y_dec'), prm.y_dec = 0;  end
% if isfield(prm,'xlims'),  xlims = prm.xlims;  else xlims = xlim;  end
% if isfield(prm,'ylims'),  ylims = prm.ylims;  else ylims = ylim;  end

% if ~isfield(prm,'y_pos_diff_a0'), prm.y_pos_diff_a0 = ylims(1)+diff(ylims)/100;  end
% if ~isfield(prm,'color_diff_a0'),            prm.color_diff_a0 = gray3;  end


% Checks
% assert(numel(EEG)==1, 'EEG is an array, should be one element.');
assert( all(EEG(1).nbchan==[EEG.nbchan]), 'Different elements of ''EEG'' have different number of channels.' );
assert( all(EEG(1).pnts==[EEG.pnts]), 'Different elements of ''EEG'' have different number of timepoints.' );


% Const
Np = numel(EEG);
Nc = size(EEG(1).data,1);
Nt = size(EEG(1).data,2);
Ne = size(EEG(1).data,3);

if ~exist('samples_oi_te','var') || isempty(samples_oi_te)
    samples_oi_t = 1:Nt;
    samples_oi_e = 1:Ne;
else
    if isequal(samples_oi_te{1},'all')
        samples_oi_t = 1:Nt;
    elseif isnumeric(samples_oi_te{1})
        samples_oi_t = samples_oi_te{1};
    else
        error('error');
    end
    
    if ischar(samples_oi_te{2}) || isnumeric(samples_oi_te{2})
        samples_oi_e = samples_oi_te{2};
    else
        error('The second field of ''samples_oi_ce'' should be char or numeric values');
    end
end

% Init
topo_cp = nan(Nc,Np,'double');



% Operation - computation of topo_cp
for p = 1:Np
    if Ne == 1 % CASE: continuous data
        topo_cp(:,p) = feval(prm.avg_mth, EEG(p).data(:,samples_oi_t),2,'omitnan');
    
    else       % CASE: epoched data
        if isequal(samples_oi_e,'all')
            topo_cp(:,p) = feval(prm.avg_mth, EEG(p).data(:,samples_oi_t,:),[2,3],'omitnan');
        elseif isnumeric(samples_oi_e)
            topo_cp(:,p) = feval(prm.avg_mth, EEG(p).data(:,samples_oi_t,samples_oi_e),[2,3],'omitnan');
        elseif ~isempty(prm.condition_field) % ischar(samples_oi_e)
            for p = 1:Np
                eoi = find(ismember({EEG(p).epoch.(prm.condition_field)}, samples_oi_e));
                topo_cp(:,p) = feval(prm.avg_mth, EEG(p).data(:,samples_oi_t,eoi),[2,3],'omitnan');
            end
        end
    end
end


% Operation - plot the mean of topo_cp
hold on;

topoplot(mean(topo_cp,2,'omitnan'), EEG(1).chanlocs ...
               ,'maplimits', prm.clims ...
               ,'conv', 'off' ...
               ,'colormap', prm.cmap ...
               ,'style', 'both' ...
               ,'electrodes', prm.electrodes ...
               ,'numcontour', linspace(prm.clims(1),prm.clims(2),prm.n_contour) ...
               ); % 'fill'  'map'
%                 cols_shift = 1; % starting from 1
%             cur_col = cmap_paired(mod( (r-1)*2+cols_shift, size(cmap_paired,1) )+1,:);
%             set(findobj(gca,'type','patch'),'facecolor',cur_col)
set(findobj(gca,'type','patch'),'facecolor', prm.col_circle)
set(gcf, 'Color', bkg_col);


%%
global EEG

%% Select a participant

p = 1:14; % Def here a participant to focus on


%% Quickly plot the ERPs across channels (for a given participant)

cond_nm = 'all';

% Go to figure 3 and clear it out.
figure(3);
clf;

% Plot: plot the ERPs across all channels
    prm = struct;
    prm.condition_field = 'eventtype';
fct_eeg_plot_fast_erp_across_channels_from_EEGstruct(EEG(p), {'all',cond_nm}, prm);
% fct_eeg_plot_fast_erp_across_channels_from_EEGstruct(EEG(p), {'all',{,cond_nm}});



% Plot: highlight the ERPs across targetted channels
chans_labels_oi = {'8L' '8R'}; % <- Def here the list of channels to highlight

    chans_labels = {EEG(p(1)).chanlocs.labels};
    chans_oi = find(ismember(chans_labels,chans_labels_oi)); %  '2LA' '2RA'

    times_rge = [-100 500];

    % Paramters to plot the erps in an highlight way
    prm = struct;
    prm.color = [0 .5 .2]; % green
    prm.lineW = 3; % bold
    prm.linealpha = .5; % more opaque

% fct_eeg_plot_fast_erp_across_channels_from_EEGstruct(EEG(p), {chans_oi,'all'}, prm);

title(cond_nm);
xlim(times_rge); ylim([-6 10]);

%% Quickly plot a animation of the scalp topography across the timepoints (for a given participant)
% Tip: quickly click on the command window and then type ctrl+c to stop the animation.

cond_nm = 'all';        n_fig_ERP = 3;
% cond_nm = 'tools';      n_fig_ERP = 4;
% cond_nm = 'hands';      n_fig_ERP = 5;
% cond_nm = 'feet';       n_fig_ERP = 6;
% cond_nm = 'animals';    n_fig_ERP = 7;

% Constants
times = EEG(p).times;
Nt = numel(times);
n_fig_topo = n_fig_ERP+10;

% Parameters
prm = struct;
prm.condition_field = 'eventtype';
prm.clims = 4*[-1 1]; % range of the color when plotting the topoplot

times_rge = [-100 500];
times_rge = [128 136];
times_rge = [156 178];
% times_rge = [160 184];
% times_rge = [180 192];
% times_rge = [236 252];
times_rge = [320 344];

times_oi = find(times>=times_rge(1) & times<=times_rge(2));
%times_oi = 1:Nt;
for t = times_oi % for each timepoint

    % Go to figure 2 and clear it out.
    figure(n_fig_topo); clf; 

    % Plot: plot the scalp topography
    fct_eeg_plot_fast_topo_from_EEGstruct(EEG(p), {t,cond_nm}, prm);
    
    % Title: the current timepoint
    title(sprintf('t = %d ms',times(t)));
        
    % Go to figure 3 and update a vertical bar.
    figure(n_fig_ERP);
    if exist('h','var'), delete(h); end, 
    h = xline(times(t),'-',sprintf('%d ms',times(t)),'color',[1 .5 0]);

    pause(.01); % little pause to control the speed
end
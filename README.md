# Quick EEG visu functions

This toolbox supplies simple function to quickly visualize EEG data.

Format accounted are EEGlab structures and EEG data in simple matrix.


## Content

The functions included are:
- Plotting a participant's EEG data [continuously](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/functions/fct_eeg_plot_fast_eegplot_from_EEGstruct.m), wrapping eegplot() from EEGLAB to handle it very simply.
- Plotting ERPs across channels from a participant's EEG epoched data, put either in [an EEGLAB structure](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/functions/fct_eeg_plot_fast_erp_across_channels_from_EEGstruct.m) or [a simple matrix](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/functions/fct_eeg_plot_fast_erp_across_channels_from_eeg_cte.m).
- Plotting the scalp topography at a given timepoint from a participant's EEG epoched data, put either in [an EEGLAB structure](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/functions/fct_eeg_plot_fast_topo_from_EEGstruct.m) or [a simple matrix](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/functions/fct_eeg_plot_fast_topo_from_topo_c.m).


## Getting started

Use the [demo script](https://gitlab.com/gabrielbesson/quick-eeg-visu-functions/-/blob/main/code/script_demo_quick_ERP_visu_per_part.m) to see examples of how to use these functions.
